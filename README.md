# webmon

WEB サイトの死活監視用(golang+aws cloudwatch)

## build

```
docker build -t webmon .
```

## Usage

* docker run -e AWS_ACCESS_KEY_ID=id -e AWS_SECRET_ACCESS_KEY=key -e AWS_DEFAULT_REGION=region webmon -u https://〜
* docker run [-e AWS_PROFILE=profile名] -v ~/.aws:/root/.aws webmon -u https://〜

など
