package main

import (
    "context"
    "fmt"
    "github.com/aws/aws-sdk-go-v2/aws"
    "github.com/aws/aws-sdk-go-v2/config"
    "github.com/aws/aws-sdk-go-v2/service/cloudwatch"
    "github.com/aws/aws-sdk-go-v2/service/cloudwatch/types"
)

func notifyCloudwatch(url *string, namespace *string, ok bool) {
    cfg, err := config.LoadDefaultConfig(context.TODO())
    if err != nil {
        fmt.Println(err)
        return
    }

    client := cloudwatch.NewFromConfig(cfg)

    var notifyCount float64
    if ok {
        notifyCount = 0
    } else {
        notifyCount = 1
    }

    input := &cloudwatch.PutMetricDataInput {
        Namespace: namespace,
        MetricData: []types.MetricDatum {
            {
                MetricName: aws.String("SiteFailed"),
                Value: aws.Float64(notifyCount),
                Unit: types.StandardUnitCount,
                Dimensions: []types.Dimension {
                    {
                        Name: aws.String("url"),
                        Value: url,
                    },
                },
            },
        },
    }

    _, err = client.PutMetricData(context.TODO(), input)

    if err != nil {
        fmt.Println(err)
    } else {
        fmt.Println("ok")
    }
}
