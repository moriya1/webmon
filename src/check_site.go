package main

import (
    "fmt"
    "net/http"
    "time"
)

func checkSite(url *string, namespace *string) {
    client := &http.Client {
        Timeout: 5 * time.Second,
    }
    resp, err := client.Get(*url)
    if err != nil {
        fmt.Println(err)
        notifyCloudwatch(url, namespace, false)
    } else {
        fmt.Println(resp.StatusCode)
        if (resp.StatusCode >= 400) {
            notifyCloudwatch(url, namespace, false)
        } else {
            notifyCloudwatch(url, namespace, true)
        }
    }
}
