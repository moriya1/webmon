package main

import (
    "fmt"
    "flag"
)

func main() {
    url := flag.String("u", "", "url")
    namespace := flag.String("n", "Webmon", "namespace")
    flag.Parse()
    if len(*url) == 0 {
        fmt.Println("specify url")
        return
    }

    checkSite(url, namespace)
}
