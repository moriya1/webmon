FROM golang:bullseye AS builder

COPY src /usr/src/website_monitor
WORKDIR /usr/src/website_monitor
RUN go build

FROM debian:bullseye
RUN apt update && apt install -y ca-certificates
WORKDIR /root
COPY --from=builder /usr/src/website_monitor/webmon .

ENTRYPOINT ["/root/webmon"]

CMD ["-h"]

